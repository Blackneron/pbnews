# PBnews - README #
---

### Overview ###

The **PBnews** script is written in Perl and collects actual news information from the "20min.ch" website and displays it on a very simple static webpage which is responsive.

### Screenshots ###

![PBnews - Desktop Computer View](development/readme/pbnews1.png "PBnews - Desktop Computer View")

![PBnews - Mobile View](development/readme/pbnews2.png "PBnews - Mobile View")

### Setup ###

* Upload the script **news.pl** to the CGI directory of your webhost.
* Edit the configuration section of the same file.
* Configure a cronjob which runs the script every couple of minutes.
* Access the static webpage **https://yourdomain/news.html** from a webbrowser.
* The news list will be displayed.

### Support ###

This is a free script and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBnews** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).

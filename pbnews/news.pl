#!/usr/bin/env perl

# Script version: 1.2

# Use variable declarations.
use strict;

# Display warnings.
use warnings;

# Use this simple wrapper to HTTP POST requests.
use LWP::Simple;

# Use this module to encode characters.
use Encode qw(decode encode);

# Turn off output buffering.
$|=1;


########################################################################
#                                                                      #
#                 C O N F I G U R A T I O N - B E G I N                #
#                                                                      #
########################################################################

# Specify if the news images are displayed (0 or 1).
my $show_images = 1;

# Specify the maximum number of news displayed (0 = All news).
my $max_news = 0;

# Specify the categories which should be displayed (uncomment the lines).
my @categories = (
  "ausland",
#  "auto",
  "digital",
#  "entertainment",
#  "finance",
#  "fitness",
#  "immobilien",
  "leben",
#  "native",
#  "panorama",
#  "people",
#  "schweiz",
  "schweiz/news",
#  "schweiz/bern",
  "schweiz/zuerich",
#  "schweiz/ostschweiz",
#  "schweiz/zentralschweiz",
#  "sport",
#  "tilllate",
  "wissen",
#  "wettbewerbe",
#  "community",
#  "tilllate.com",
#  "sponsored",
#  "www.friday-magazine.ch",
);

# Specify the website address (eample: http://www.20min.ch).
my $site = "http://www.20min.ch";

# Specify the full path of the temporary data file - localhost.
my $localpath = "/home/brigitta/Patrick/Perl";

# Specify the full path of the temporary data file - remote location.
my $remotepath = "/var/www/virtual/pbsoft/html";

# Specify if the Perl version should be displayed on the status page (0 or 1).
my $show_version = 1;


########################################################################
#                                                                      #
#                   C O N F I G U R A T I O N - E N D                  #
#                                                                      #
########################################################################


# Specify the main part of the perl script.
sub main {

  # Initialize the news counter.
  my $number = 0;

  # Initialize the active categories string.
  my $active_categories = "";

  # Initialize the string which is used to store the new image location.
  my $image_new = "";

  # Initialize the string which is used to store the old image location.
  my $image_old = "";

  # Initialize the input path.
  my $input = "";

  # Initialize the output path.
  my $output = "";

  # Initialize the location.
  my $location = "local";

  # Check if the file path exist - localhost.
  if(-d $localpath) {

    # Specify location of the temporary data file - local.
    $input = $localpath."/input.tmp";

    # Specify the location of the output file - local.
    $output = $localpath."/news.html";
  }
  else { # The local file path does not exist.

    # Specify location of the temporary data file - remote location.
    $input = $remotepath."/temp/input.tmp";

    # Specify the location of the output file - remote location.
    $output = $remotepath."/news.html";

    # Specify the location.
    $location = "remote";

  } # The local file path does not exist.


  ######################################################################
  # Create the active category string
  ######################################################################

  # Loop through the active categories.
  foreach my $item (@categories) {

    # Check if the actual item is not the first one.
    if($active_categories ne "") {

      # Add a separator to the string.
      $active_categories .= ", ";
    }

    # Add the actual category to the string.
    $active_categories .= ucfirst($item);

  } # Loop through the active categories.


  ######################################################################
  # Display the HTML header for this update script
  ######################################################################

  # Specify the content type for the screen output.
  print "Content-type: text/html\n\n";

  # Display the HTML header - Begin.
  print "<!DOCTYPE html>\n";
  print "<html lang=\"de\">\n";
  print "  <head>\n";
  print "    <meta charset=\"utf-8\">\n";
  print "    <title>20 Minuten - Update News</title>\n";

  # Insert the CSS code.
  print "    <style media=\"screen\" type=\"text/css\">\n";
  print "      body{font-family:Arial,Helvetica,sans-serif;font-size:100%;color:#036;margin-top:50px;background-color:#CBDDED;text-align:center}\n";
  print "    </style>\n";

  # Display the HTML header - End.
  print "  </head>\n";
  print "  <body>\n";

  # Display an information message.
  print "    <h2>Updating the news content...</h2>\n";


  ######################################################################
  # Get the website data
  ######################################################################

  # Store the data from the website into a temporary file.
  my $data = getstore($site, $input);

  # Check if the data could be received.
  if($data == 200) {


    ####################################################################
    # Open necessary files
    ####################################################################

    # Open the temporary file to process the content line by line.
    open(INPUT, $input) or die ("Input file '$input' not found!\n");

    # Open the output file in which the processed data will be stored.
    open(OUTPUT, '>'.$output) or die ("Can't create '$output!'\n");


    ####################################################################
    # Create the actual datestring
    ####################################################################

    # Get the actual date and time information into variables.
    (my $sec, my $min, my $hour, my $mday, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime();

    # Create the string with the actual date and time.
    my $datestring = sprintf("%02d.%02d.%04d / %02d:%02d:%02d", $mday, $mon, ($year += 1900), $hour, $min, $sec);


    ####################################################################
    # Insert the HTML header into the news page
    ####################################################################

    # Insert the HTML header into the output file - Begin.
    print OUTPUT "<!DOCTYPE html>\n";
    print OUTPUT "<html lang=\"de\">\n";
    print OUTPUT "  <head>\n";
    print OUTPUT "    <meta charset=\"utf-8\">\n";
    print OUTPUT "    <title>20 Minuten News - Updated am $datestring</title>\n";

    # Insert the CSS code into the output file.
    print OUTPUT "    <style media=\"screen\" type=\"text/css\">\n";
    print OUTPUT "      body{font-family:Arial,Helvetica,sans-serif;background-color:#CBDDED;text-align:center;}\n";
    print OUTPUT "      img.logo{height:auto;border:none;}\n";
    print OUTPUT "      img.news{width:98%;height:auto;border:solid 2px #94ACC2;}\n";
    print OUTPUT "      div.title{background-image:url('back.gif');background-color:#AFCBE3;border:solid 2px #8AB2D6;}\n";
    print OUTPUT "      div.news{background-color:#D5E4F1;border:solid 2px #AFCBE3;}\n";
    print OUTPUT "      div.title,div.news{box-shadow: 8px 8px 5px #4B7AAA;border-radius:10px;margin-left:auto;margin-right:auto;}\n";
    print OUTPUT "      h1,h2,h3,a{color:#036}\n";
    print OUTPUT "      a:hover{color:#2174BD;}\n";
    print OUTPUT "      h2.categories{margin-bottom:5px;}\n";
    print OUTPUT "      h3.categories{margin-bottom: 0;margin-top:5px;}\n";
    print OUTPUT "      span.number,h3.categories{color:#C03;}\n";
    print OUTPUT "      \@media(max-width:767px){h1{font-size:2.0em;margin-top: 10px;}h2{font-size:1.8em;margin-top:5px;}h3{font-size:1.6em;}div.title,div.news{width:95%;margin-bottom:20px;}div.title{padding:10px 4px 4px 4px;margin-top:20px;}div.news{padding:4px;}img.logo{width:60px;}img.news{width:98%;}}\n";
    print OUTPUT "      \@media(min-width:768px) and (max-width:991px){h1{font-size:2.0em;margin-top: 15px;}h2{font-size:1.8em;margin-top:10px;}h3{font-size:1.6em;}div.title,div.news{width:95%;margin-bottom:20px;}div.title{padding:15px 6px 6px 6px;margin-top:20px;}div.news{padding:6px;}img.logo{width:60px;}img.news{width:98%;}}\n";
    print OUTPUT "      \@media(min-width:992px) and (max-width:1199px){h1{font-size:1.8em;margin-top: 20px;}h2{font-size:1.6em;margin-top:15px;}h3{font-size:1.4em;}div.title,div.news{width:95%;margin-bottom:25px;padding:20px 8px;}div.title{margin-top:25px;}img.logo{width:70px;}img.news{width:50%;}}\n";
    print OUTPUT "      \@media(min-width:1200px){h1{font-size:1.8em;margin-top: 25px;}h2{font-size:1.6em;margin-top:20px;}h3{font-size:1.4em;}div.title,div.news{width:60%;margin-bottom:30px;padding:30px 10px;}div.title{margin-top:30px;}img.logo{width:80px;}img.news{width:60%;}}\n";
    print OUTPUT "    </style>\n";

    # Insert the HTML header into the output file - End.
    print OUTPUT "  </head>\n";


    ####################################################################
    # Insert the HTML body into the news page - Begin
    ####################################################################

    # Insert the HTML body into the output file - Begin.
    print OUTPUT "  <body>\n";

    # Open the title box.
    print OUTPUT "    <div class=\"title\">\n";

    # Display the logo.
    print OUTPUT "      <a href=\"http://www.20min.ch\" onclick=\"window.open(this.href, '20 Minuten - Home').focus(); return false;\" >\n";
    print OUTPUT "      <img class=\"logo\" src=\"http://www.20min.ch/2010/img/navigation/20min_logo.png\" alt=\"20 Minuten News\">\n";
    print OUTPUT "      </a>\n";

    # Display the page title.
    print OUTPUT "      <h1>News vom $datestring</h1>\n";

    # Display the active categories.
    print OUTPUT "      <h2 class=\"categories\">Angezeigte Kategorien:</h2>\n";
    print OUTPUT "      <h3 class=\"categories\">$active_categories</h3>\n";

    # Close the title box.
    print OUTPUT "    </div>\n";


    ####################################################################
    # Process the website data to get the necessary links and images
    ####################################################################

    # Loop through the lines of the temporary file.
    while(my $line = <INPUT>) {

      # Initialize the show flag.
      my $show = 0;

      # Check if the actual line should be processed and the line contains a header with link or an image.
      if(($line =~ /<(h|H)(1|2)\b/ && $line =~ /href=\"/) || ($line =~ /<img src=\"/ && $line =~ /teaserbreit/)) {

        # Loop through the categories.
        foreach my $item (@categories) {

          # Check if the actual information should be displayed.
          if ($line =~ /\/$item\//ig) {

            # Enable the show flag.
            $show = 1;
          }

        } # Loop through the categories.


        ################################################################
        # Process the found links and images
        ################################################################

        # Check if the actual line should be displayed.
        if($show == 1) {

          # Trim the line at the beginning and end.
          $line =~ s/^\s+|\s+$//g;


          ##############################################################
          # If necessary encode the output for the remote location
          ##############################################################

          # Check if the location is the remote location.
          if ($location eq "remote") {

            # Encode the actual line to UTF8.
            $line = encode('UTF-8', $line, Encode::FB_CROAK);
          }


          ##############################################################
          # Accomodate links and images
          ##############################################################

          # Check if the actual line does not contain an image.
          if($line !~ /<img src=\"/) {

            # Increase the news counter.
            $number += 1;

            # Replace the headers h1 and h2 with a h1 header and add the news number.
            $line =~ s/<(h|H)(1|2)[-a-zA-Z0-9?.&:'; ="\/]*>/<h2><span class=\"number\">$number<\/span> - /ig;
            $line =~ s/<\/(h|H)(1|2)>/<\/h2>/ig;
          }
          else { # The actual line contains an image.

            # Remove the CSS class from the image.
            $line =~ s/<div class=\"teaser_image size\"[-a-zA-Z0-9?.&:'; ="\/]*>//;

            # Remove the width parameter of the image.
            $line =~ s/width=\"[0-9]*\"//;

            # Remove the height parameter of the image.
            $line =~ s/height=\"[0-9]*\"//;

            # Add a new CSS class to the image.
            $line =~ s/<img src=/<img class="news" src=/ig;

          } # The actual line contains an image.


          ##############################################################
          # Check if the maximum number of news messages was displayed
          ##############################################################

          # Check if the maximum number of news is displayed.
          if($number > $max_news && $max_news > 0) {

            # Exit the loop.
            last;
          }


          ##############################################################
          # Add javascript code to open the links in a new window
          ##############################################################

          # Check if the line contains a link.
          if($line =~ /(href=[a-z0-9\/\-:.?;"]+)/ig) {

            # Check if the there was a match.
            if(defined $1) {

              # Get the link string.
              my $link = $1;

              # Add the javascript code for opening the website in a new tab/window.
              $line =~ s/$link/$link onclick=\"window.open(this.href, 'News').focus(); return false;\"/ig;
            }
          } # Check if the line contains a link.


          ##############################################################
          # Make some minor corrections
          ##############################################################

          # Replace multiple spaces with one space.
          $line =~ s/[ ]{2,}/ /;

          # Replace & with &amp;.
          if($line !~ /(&ndash;|&nbsp;)/) {
            $line =~ s/&/&amp;/;
          }

          # Check if the link does not contain the full URL.
          if($line !~ /href=\"http/) {

            # Add the website URL to the link.
            $line =~ s/href=\"/href=\"$site/ig;
          }


          ##############################################################
          # Print the link or image into the news page
          ##############################################################

          # Check if the actual line does not contain an image.
          if($line !~ /<img class="news" src=\"/) {

            # Open the news box.
            print OUTPUT "    <div class=\"news\">\n";

            # Insert the news title into the output file.
            print OUTPUT "      ".$line."\n";

            # Check if the actual image was not inserted into the output file before.
            if($image_new ne $image_old && $show_images == 1) {

              # Insert the image.
              print OUTPUT "      ".$image_new."\n";
            }

            # Close the news box.
            print OUTPUT "    </div>\n";

          } # Check if the actual line does not contain an image.

          else { # The actual line does contain an image.

            # Store the actual image link for later use.
            $image_old = $image_new;

            # Store the new image.
            $image_new = $line;

          } # The actual line does contain an image.

        } # Check if the actual line should be displayed.

      } # Check if the actual line should be processed and the line contains a header with link or an image.

    } # Loop through the lines of the temporary file.


    ####################################################################
    # Insert the HTML footer into the news page
    ####################################################################

    # Insert the HTML body into the output file - End.
    print OUTPUT "  </body>\n";
    print OUTPUT "</html>\n";


    ####################################################################
    # Close the necessary files
    ####################################################################

    # Close the input file.
    close(INPUT);

    # Close the output file.
    close(OUTPUT);


    ####################################################################
    # Display a status message
    ####################################################################

    # Display a success message.
    print "    <h3>The content was updated successfully!</h3>\n";
    print "    <h4>$datestring</h4>\n";
  }
  else { # The data could not be received.

    # Display an error message.
    print "    <h3>The content could NOT be updated!</h3>\n";
  }


  ######################################################################
  # Display the Perl version (if enabled)
  ######################################################################

  # Check if the Perl version should be displayed.
  if($show_version == 1) {

    # Display the actual Perl version.
    print "    <h4>Perl version: $^V</h4>\n";
  }


  ######################################################################
  # Display the HTML footer for this update script
  ######################################################################

  # Display the HTML end.
  print "  </body>\n";
  print "</html>\n";
}


########################################################################
# Run all the code
########################################################################

# Call the main routine to execute the code.
main();

